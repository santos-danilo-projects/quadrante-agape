import { Component, OnInit } from '@angular/core';
import { GmapsApiService } from './gmaps-api/gmaps-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'app works!';
  
  constructor(
    private gmaps:GmapsApiService
  ){

  }

  ngOnInit(){
    this.gmaps.addressToGeocoding("Q. 3 Conjunto F, 29 - Sobradinho");
  }
}
