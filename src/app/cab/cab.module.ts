import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
//
import { CabComponent } from './cab.component';

const cabRoutes: Routes = [
  { path: 'cab', component: CabComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild( cabRoutes )
  ],
  declarations: [CabComponent],
  exports: [
    RouterModule
  ]
})
export class CabModule { }
