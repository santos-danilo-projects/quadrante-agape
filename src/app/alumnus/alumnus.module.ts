import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
//
import { RepositoryService } from '../shared/service/repository.service';
import { AlumnusService } from './alumnus.service'
import { DataComponent } from './data/data.component';
import { AlumnusComponent } from './alumnus/alumnus.component';
import { FormComponent } from './form/form.component';

const authRoutes: Routes = [
  { path: 'alumnus/create', component: FormComponent }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild( authRoutes )
  ],
  declarations: [
    DataComponent,
    AlumnusComponent,
    FormComponent
  ],
  exports: [
    AlumnusComponent,
    RouterModule
  ],
  providers: [
    RepositoryService,
    AlumnusService
  ]
})

export class AlumnusModule { }
