import { Component, OnInit } from '@angular/core';
import { AlumnusService } from '../alumnus.service'
import { Alumnus } from '../../shared/models/alumnus.model';

@Component({
  selector: 'app-alumnus',
  templateUrl: './alumnus.component.html',
  styleUrls: ['./alumnus.component.css'],
  providers: [AlumnusService]
})
export class AlumnusComponent implements OnInit {
  alumnus: Alumnus[];
  erroMsg: string;
  selectedAlumnus: Alumnus;

  constructor(
    private service: AlumnusService
  ){
  }

  ngOnInit() {
    this.service.retriveAll().subscribe(
      data => { 
                this.alumnus = data; 
      },
      error => {
              this.erroMsg = error;
      }
    );
    console.log(this.alumnus);
  }

  showData(alumnus){
    this.selectedAlumnus = alumnus;
  }

}
