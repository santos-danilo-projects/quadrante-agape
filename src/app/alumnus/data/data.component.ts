import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Alumnus } from '../../shared/models/alumnus.model';

@Component({
  selector: 'alumnus-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})

export class DataComponent implements OnInit {
  private activeModal: boolean;
  @Input() a: Alumnus;

  constructor() { 
    this.activeModal = false;
    this.a = new Alumnus("Danilo", 20);
  }

  ngOnInit() {
  }

}
