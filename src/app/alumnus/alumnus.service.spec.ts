import { TestBed, inject } from '@angular/core/testing';

import { AlumnusService } from './alumnus.service';

describe('AlumnusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlumnusService]
    });
  });

  it('should ...', inject([AlumnusService], (service: AlumnusService) => {
    expect(service).toBeTruthy();
  }));
});
