import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Rx";

import { RepositoryService } from '../shared/service/repository.service';
import { Alumnus } from '../shared/models/alumnus.model';

@Injectable()
export class AlumnusService extends RepositoryService<Alumnus> {
  private uriResource: string = '/alumnus';

  alumnus: Alumnus[] = [];

  retriveAll(){
    let b = new Alumnus("Danilo Santos", 21);
    let c = new Alumnus("Kamila Teles", 17);
    let d = new Alumnus("Ana Karoline Menezes", 22);

    this.alumnus.push(b, c, d);
    return Observable.of( this.alumnus );
    //return super.get(this.uriResource);
  }

  insert(alumnus): Observable<Alumnus>{
    this.alumnus.push(alumnus);

    return Observable.of( alumnus );
  }
}
