import { Component, OnInit } from '@angular/core';
import { FormControl, 
         FormBuilder, 
         FormGroup, 
         Validators } from '@angular/forms';
import { AlumnusService } from '../alumnus.service';
import { Alumnus } from '../../shared/models/alumnus.model';

@Component({
  selector: 'alumnus-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  private activeModal: boolean;
  private form: FormGroup;
  
  constructor(
    private formBuiler: FormBuilder,
    private service: AlumnusService  
  ){ 
    this.activeModal = false;
    this.form = formBuiler.group({
      'name': ['', [Validators.required] ],
      'age': ['', [Validators.required ]]
    });
  }

  ngOnInit() {
  }

  toogleModal(){
    this.activeModal = (!this.activeModal);
  }

  insert(form: FormGroup){
    if( !form.valid ){
      console.log("Form invalid");
    }else{
      //form valid
      let a: Alumnus = new Alumnus();
      Object.assign(a, form.value);

      this.service.insert(a).subscribe(
        next => { 
          console.log(next);
        },
        error => {
          console.log(error);
        }
      );

    }
  }
}
