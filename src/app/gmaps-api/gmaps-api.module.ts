import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GmapsApiService } from './gmaps-api.service';
import { HttpUtilService } from '../shared/service/http-util.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports: [],
  providers: [
    GmapsApiService,
    HttpUtilService
  ]
})
export class GoogleMapsApiModule { }
