import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { HttpUtilService } from '../shared/service/http-util.service';

@Injectable()
export class GmapsApiService {
  private GEO_CODING_KEY: string = 'AIzaSyAN05SOUDZOop521aN9KNT0K76t2AtuV2U';
  private GEO_CODING_URL: string = 'https://maps.googleapis.com/maps/api/geocode/';
  private outputFormat: string = 'json?';

  constructor(
    private http: Http,
    private httpUtil: HttpUtilService
  ) {}

  addressToGeocoding(address){
    address = address.replace(/\s/g, '+');
    
    let path:string = ('address=' + address);
    let url: string = this.generateUrl(path);
    console.log( url );
    /*
    return this.http.get( url )
                    .map( this.httpUtil.extractData )
                    .catch( this.httpUtil.handleError );
    */
  }

  generateUrl(path?:string): string{
    let url:string = (
      this.GEO_CODING_URL +
      this.outputFormat +
      path +
      '&region=pt-br' +    
      '&key=' + this.GEO_CODING_KEY);
    
    return url;
  }
}
