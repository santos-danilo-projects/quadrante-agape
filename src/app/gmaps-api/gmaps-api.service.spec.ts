import { TestBed, inject } from '@angular/core/testing';

import { GmapsApiService } from './gmaps-api.service';

describe('GmapsApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GmapsApiService]
    });
  });

  it('should ...', inject([GmapsApiService], (service: GmapsApiService) => {
    expect(service).toBeTruthy();
  }));
});
