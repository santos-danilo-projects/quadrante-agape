import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileBoardComponent } from './tile-board.component';

describe('TileBoardComponent', () => {
  let component: TileBoardComponent;
  let fixture: ComponentFixture<TileBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
