import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from '../shared/template/index';
import { PageNotFoundComponent } from '../shared/page-not-found/page-not-found.component';
import { AlumnusComponent } from '../alumnus/alumnus/alumnus.component';
import { TileBoardComponent } from '../tile-board/tile-board.component';

const appRoutes: Routes = [
  { path: 'board', component: TileBoardComponent },
  { path: 'alumnus', component: AlumnusComponent },
  { path: '', redirectTo: '/board', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot( appRoutes )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})

export class RoutingModule { }
