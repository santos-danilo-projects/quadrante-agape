import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
//angular imports above
import { Auth } from './auth.model';
import { RepositoryService } from '../shared/service/repository.service'

@Injectable()
export class AuthService extends RepositoryService<Auth> {

    constructor(http: Http) {
        super(http);
    }
}