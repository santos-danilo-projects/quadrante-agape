import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  private authForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.authForm = formBuilder.group({
      'user': [null, [Validators.required]],
      'password': [null, [Validators.required]]
    });
  }

  ngOnInit() {
  }

  OnSubmit(form: FormGroup){
    console.log(form.valid);
  }

}
