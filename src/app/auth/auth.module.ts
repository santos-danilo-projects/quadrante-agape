import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AuthComponent } from './auth.component';

const authRoutes: Routes = [
  { path: 'auth', component: AuthComponent }
];

@NgModule({
  declarations: [
    AuthComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild( authRoutes )
  ],
  exports: [
    AuthComponent,
    RouterModule
  ]
  
})
export class AuthModule { }
