import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { RoutingModule } from './routing/routing.module';
import { AlumnusModule } from './alumnus/alumnus.module';
import { CabModule } from './cab/cab.module';
import { AuthModule } from './auth/auth.module';
import { GoogleMapsApiModule } from './gmaps-api/gmaps-api.module';

import { HeaderComponent,
         FooterComponent,
         NavbarComponent } from './shared/template/index';

import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { TileBoardComponent } from './tile-board/tile-board.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlumnusModule,
    AuthModule,
    CabModule,
    GoogleMapsApiModule,
    RoutingModule,
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    PageNotFoundComponent,
    TileBoardComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
