import { Alumnus } from './alumnus.model';

export class Perseverare{
    year: Date;
    alumnus: Alumnus;
}