export class Alumnus{
    name: String;
    birthDate: Date;
    email: string;
    
    //table
    //name 1 x n phone
    phone: String[];

    address: string;
    latitude: string;
    longitude: string;

    father: string;
    mother: string;
    sacrament: string[];
    age: Number;
    //Multi-valoradora, entretanto separado por virgula
    knownPeople?: String[];
    community: String;
    allergicRemedy: string;
    allergicFood: string;

    constructor(name?:string, age?:number){
        this.name = name || '';
        this.age = age || 0;
    }
}