import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class RepositoryService<T> {
  static hostUrl: string = 'http://localhost:3000';

  constructor(
    private http: Http
  ){}

  protected get(path: string): Observable<T>{
    let url = RepositoryService.hostUrl + path;
    
    return this.http.get(url, this.requestOptions)
                    .map( this.extractData )
                    .catch( this.handleError );
  }
  protected post(path: string, body: object): Observable<T>{
    return this.http.post(path, body, this.requestOptions)
                      .map( this.extractData )
                    .catch( this.handleError );
  }

  protected put(path: string, body: object): Observable<T>{
    return this.http.put(path, body, this.requestOptions)
                    .map( this.extractData )
                    .catch( this.handleError );
  }

  protected delete(path: string): Observable<T>{
    return this.http.delete(path, this.requestOptions)
                    .map( this.extractData )
                    .catch( this.handleError );
  }
  
  requestOptions(): RequestOptions{
    let headers = new Headers( {'Content-Type': 'application/json',} );
    headers.append("Access-Control-Allow-Origin", "*");
    let options = new RequestOptions( {headers: headers} );
    return options;
  }

  extractData(res: Response){
      let bodyResponse = res.json();
      return bodyResponse.data || {};
  }

  handleError(error: Response | any){
    let errMsg: String;

    if( error instanceof Response ){
      const bodyResponse = error.json() || '';
      const err = bodyResponse.error || JSON.stringify(bodyResponse);
      errMsg = `${error.status} - ${error.statusText || '' } ${err}`;
    }else{
      errMsg = error.message ? error.message : error.toString();
    }
    
    console.log( errMsg );
    return Observable.throw( errMsg );
  }
}
