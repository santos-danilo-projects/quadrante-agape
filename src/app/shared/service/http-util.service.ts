import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class HttpUtilService {

  constructor() { }
    
  requestOptions(): RequestOptions{
    let headers = new Headers( {'Content-Type': 'application/json',} );
    headers.append("Access-Control-Allow-Origin", "*");
    let options = new RequestOptions( {headers: headers} );
    return options;
  }

  extractData(res: Response){
      let bodyResponse = res.json();
      return bodyResponse.data || {};
  }

  handleError(error: Response | any){
    let errMsg: String;

    if( error instanceof Response ){
      const bodyResponse = error.json() || '';
      const err = bodyResponse.error || JSON.stringify(bodyResponse);
      errMsg = `${error.status} - ${error.statusText || '' } ${err}`;
    }else{
      errMsg = error.message ? error.message : error.toString();
    }
    
    console.log( errMsg );
    return Observable.throw( errMsg );
  }

}
