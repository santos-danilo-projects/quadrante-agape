import { QuadranteAgapePage } from './app.po';

describe('quadrante-agape App', () => {
  let page: QuadranteAgapePage;

  beforeEach(() => {
    page = new QuadranteAgapePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
